package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

func main() {

	fmt.Println("Starting nytimes server v2")
	defer func() { fmt.Println("Stopping server") }()
	http.HandleFunc("/svc/movies/v2/reviews/search.json", reviewHandler)
	http.HandleFunc("/svc/movies/v2/critics/all.json", criticsHandler)
	http.HandleFunc("/", omdbHandler)
	http.HandleFunc("/omdb/", omdbHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func omdbFilename(s string) string {

	return fmt.Sprintf("omdb-data/%s.json", url.PathEscape(s))
}

func omdbHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("OMDB")
	start := time.Now()
	title := r.URL.Query().Get("t")

	fmt.Printf("Searching for title [%s]\n", title)
	// Put in some wait time
	dat, err := ioutil.ReadFile(omdbFilename(title))
	if err != nil {
		fmt.Fprintf(w, "boom")
	} else {
		w.Write(dat)
	}

	fmt.Printf("Search title took %s", time.Since(start))
}

func criticsHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	dat, err := ioutil.ReadFile("server/critics.json")
	fmt.Println("Searching for all critics")
	if err != nil {
		fmt.Fprintf(w, "boom")
	} else {
		w.Write(dat)
	}
	fmt.Printf("List Critics took %s", time.Since(start))
}

func reviewHandler(w http.ResponseWriter, r *http.Request) {

	start := time.Now()
	reviewer := r.URL.Query().Get("reviewer")
	offsetStr := r.URL.Query().Get("offset")

	var offset int
	i, err := strconv.Atoi(offsetStr)
	if err == nil {
		offset = i
	}

	fmt.Printf("Searching for reviewer [%s] offset [%d]\n", reviewer, offset)
	// Put in some wait time
	dat, err := ioutil.ReadFile(filename(reviewer, offset))
	if err != nil {
		fmt.Fprintf(w, "boom")
	} else {
		w.Write(dat)
	}

	fmt.Printf("Execution took %s", time.Since(start))
}

func filename(critic string, offset int) string {

	return fmt.Sprintf("server/%s-%d.json", critic, offset)
}
