
# Accept the Go version for the image to be set as a build argument.
# Default to Go 1.11
ARG GO_VERSION=1.11

# First stage: build the executable.
FROM golang:${GO_VERSION}-alpine AS builder
WORKDIR /nytimes

#Copy all the things
COPY . ./
#Build the thing
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main  

FROM scratch
EXPOSE 8080
COPY --from=builder /nytimes/. .
CMD ["./main"]



# FROM golang
# WORKDIR /src
# ADD . ./
# RUN CGO_ENABLED=0 go build -o main
# ENV PORT 8080
# CMD [“/main”]
